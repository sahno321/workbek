const logger = require("../config/logger");
const userService = require("../service/User.service");

async function validateUserData(req, res, next) {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send("Email and password are required");
  }
  const emailRegex = /\S+@\S+\.\S+/;
  if (!emailRegex.test(email)) {
    return res.status(400).send("Invalid email format");
  }

  if (password.length < 4 || password.length > 16) {
    return res
      .status(400)
      .send("Password must be between 4 and 16 characters long");
  }

  next();
}
module.exports = validateUserData;
