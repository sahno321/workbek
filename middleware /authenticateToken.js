const jwt = require("jsonwebtoken");
const logger = require("../config/logger");

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(" ")[1];

  if (!token) {
    return res.status(401).send("Unauthorized: Token is missing");
  }

  jwt.verify(token, "prod", (err, decoded) => {
    if (err) {
      logger.error("Error verifying token:", err);
      console.log(token);
      return res.status(403).send("Forbidden: Invalid token");
    }
    req.email = decoded.email;
    next();
  });
};

module.exports = authenticateToken;
