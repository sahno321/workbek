const bd = require("../models/index");

const roleService = {
  async findRoleId(role) {
    const foundRole = await bd.Role.findOne({ where: { name: role } });
    return foundRole.id;
  },
};

module.exports = roleService;
