const bd = require("../models/index");
const bcrypt = require("bcrypt");
const roleService = require("./Role.service");
const newRoleId = "Candidate";

const userService = {
  async createUser(userData) {
    const password = userData.password;
    const hashedPassword = await bcrypt.hash(
      password,
      parseInt(process.env.SALT_ROUNDS),
    );
    const rolesId = await roleService.findRoleId(newRoleId);
    return await bd.User.create({
      firstName: userData.firstName,
      lastName: userData.lastName,
      email: userData.email,
      password: hashedPassword,
      RoleId: rolesId,
    });
  },
  async findUserByEmail(email) {
    return await bd.User.findOne({
      where: { email: email },
      attributes: ["email", "password"],
    });
  },
};

module.exports = userService;
