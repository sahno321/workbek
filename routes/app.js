const express = require("express");
const router = express.Router();
const userController = require("../controller/user.controller");
const validateUserData = require("../middleware /validateUserData");
const authenticateToken = require("../middleware /authenticateToken");

router.get("/", (req, res) => {
  res.send("Sveiki, pasaule!");
});

router.post("/add", validateUserData, userController.createUser);

router.get("/auth", authenticateToken, (req, res) => {
  res.send("This is a protected endpoint");
});

router.post("/login", validateUserData, userController.loginUser);

module.exports = router;
