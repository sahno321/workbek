const fs = require('fs');
const path = require('path');

const modelsDir = path.join(__dirname);
const bd = {};

fs.readdirSync(modelsDir)
    .filter(file => file.endsWith('.js') && file !== 'index.js')
    .forEach(file => {
        const model = require(path.join(modelsDir, file));
        bd[model.name] = model;
    });

module.exports = bd;
