module.exports = {
  up: async (queryInterface, Sequelize) => {
    const rolesData = [
      {
        name: "Admin",
        description: "Admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "User",
        description: "User",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Candidate",
        description: "Candidate",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    await queryInterface.bulkInsert("Roles", rolesData, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("Roles", null, {});
  },
};
