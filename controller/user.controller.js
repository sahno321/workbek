const userService = require("../service/User.service");
const logger = require("../config/logger");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const generateToken = (email) => {
  return jwt.sign({ email }, "prod", {
    expiresIn: "1h",
  });
};

const userController = {
  async createUser(req, res) {
    const userData = req.body;
    // const generateToken = (email) => {
    //   return jwt.sign({ email }, "prod", {
    //     expiresIn: "1h",
    //   });
    // };
    try {
      const emailUser = await userService.findUserByEmail(userData.email);
      if (emailUser) {
        logger.error("User with this email already exists");
        return res.status(400).send("User with this email already exists");
      }
      await userService.createUser(userData);
      logger.info("User created successfully");
      const token = generateToken(userData.email);
      return res.status(200).json({ token });
    } catch (error) {
      logger.error("Error creating user:", error);
      return res.status(500).send("Internal Server Error");
    }
  },
  async loginUser(req, res) {
    const userData = req.body;
    const emailUser = await userService.findUserByEmail(userData.email);
    try {
      if (!emailUser) {
        return res.status(401).send("Unauthorized: User not found");
      }
      const isValidPassword = await bcrypt.compare(
        userData.password,
        emailUser.password,
      );
      if (!isValidPassword) {
        return res.status(401).send("Unauthorized: Invalid password");
      }
      const token = generateToken(userData.email);
      return res.status(200).json({ token });
    } catch (error) {
      logger.error("Error creating user:", error);
      return res.status(500).send("Internal Server Error");
    }
  },
};

module.exports = userController;
