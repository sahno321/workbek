require("dotenv").config();
const express = require("express");
const app = express();
const router = require("./routes/app");
require("./config/database");

app.use(express.json());
app.use("/", router);

app.listen(process.env.PORT || 7000, () => {
  console.log(`Server start on localhost:${process.env.PORT || 7000}`);
});
